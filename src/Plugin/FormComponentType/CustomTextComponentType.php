<?php

namespace Drupal\flexiform\Plugin\FormComponentType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field_ui\Form\EntityDisplayFormBase;
use Drupal\flexiform\FormComponent\FormComponentTypeCreateableBase;
use Drupal\flexiform\Utility\Token;
use Drupal\token\TreeBuilderInterface;

/**
 * Plugin for field widget component types.
 *
 * @FormComponentType(
 *   id = "custom_text",
 *   label = @Translation("Custom Text"),
 *   component_class = "Drupal\flexiform\Plugin\FormComponentType\CustomTextComponent",
 * )
 */
class CustomTextComponentType extends FormComponentTypeCreateableBase {

  /**
   * {@inheritdoc}
   */
  public function componentRows(EntityDisplayFormBase $form_object, array $form, FormStateInterface $form_state) {
    $rows = [];
    foreach ($this->getFormDisplay()->getComponents() as $component_name => $options) {
      if (isset($options['component_type']) && $options['component_type'] == $this->getPluginId()) {
        $rows[$component_name] = $this->buildComponentRow($form_object, $component_name, $form, $form_state);
      }
    }

    return $rows;
  }

  /**
   * {@inheritdoc}
   */
  public function addComponentForm(array $form, FormStateInterface $form_state) {
    $form['content'] = [
      '#title' => t('Content'),
      '#type' => 'text_format',
      '#required' => TRUE,
    ];

    if (\Drupal::moduleHandler()->moduleExists('token')) {
      $tree = [];
      $token_info = \Drupal::service('flexiform.token')->getInfo();
      /** @var TreeBuilderInterface $tree_builder */
      $tree_builder = \Drupal::service('flexiform.token.tree_builder');
      foreach ($this->getFormEntityManager($form, $form_state)->getContextDefinitions() as $namespace => $context_definition) {
        $entity_type_id = $context_definition->getDataType();
        list(,$entity_type_id) = explode(':', $entity_type_id, 2);

        if ($namespace == '') {
          $namespace = 'base_entity';
        }

        $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
        $token_type = $entity_type->get('token_type') ?: (!empty($token_info['types'][$entity_type_id]) ? $entity_type_id : FALSE);
        if ($token_type) {
          $tree[$namespace] = $token_info['types'][$token_type];
          $tree[$namespace]['tokens'] = $tree_builder->buildTree($token_type, [
            'parents' => [$namespace]
          ]);
        }
      }

      $form['tokens'] = [
        '#type' => 'token_tree_table',
        '#token_tree' => $tree,
        '#show_restricted' => TRUE,
        '#theme_wrappers' => ['form_element']
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function addComponentFormSubmit(array $form, FormStateInterface $form_state) {
    $options = $form_state->getValue($form['#parents']);
    $options['format'] = $options['content']['format'];
    $options['content'] = $options['content']['value'];
    $form_state->setValue($form['#parents'], $options);
  }

}
