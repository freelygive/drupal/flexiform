<?php

namespace Drupal\flexiform_wizard\Routing;

use Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider;

/**
 * Defines HTML route provider for flexiform wizard entities.
 */
class WizardHtmlRouteProvider extends DefaultHtmlRouteProvider {
}
