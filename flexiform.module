<?php

/**
 * @file
 * Allow multiple entities to be used in an entity form display.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\flexiform\FlexiformEntityFormDisplay;
use Drupal\inline_entity_form\Element\InlineEntityForm;

/**
 * Implements hook_hook_info().
 */
function flexiform_hook_info() {
  $hooks['flexiform_form_entity_entity_create'] = [
    'group' => 'flexiform',
  ];
  return $hooks;
}

/**
 * Implements hook_entity_type_build().
 */
function flexiform_entity_type_build(array &$entity_types) {
  $entity_form_display_type = $entity_types['entity_form_display'];
  $entity_form_display_type->set('original_class', $entity_form_display_type->getClass());
  $entity_form_display_type->setClass('Drupal\flexiform\FlexiformEntityFormDisplay');
  $entity_form_display_type->setStorageClass('Drupal\flexiform\FlexiformEntityFormDisplayStorage');
  $entity_form_display_type->setFormClass('edit', 'Drupal\flexiform\Form\FlexiformEntityFormDisplayEditForm');
}

/**
 * Implements hook_element_info_alter().
 */
function flexiform_element_info_alter(array &$types) {
  foreach ($types as &$type) {
    $method = 'processAjaxForm';
    $has_method = FALSE;

    if (!empty($type['#process'])) {
      foreach ($type['#process'] as $process) {
        if ($process[1] == $method) {
          $has_method = TRUE;
          break;
        }
      }

      if ($has_method) {
        $type['#process'][] = 'flexiform_ajax_form_process';
      }
    }
  }

  // Handle inline_entity_form stuff.
  if (isset($types['inline_entity_form'])) {
    $types['inline_entity_form']['#ief_element_submit'] = [
      'flexiform_ief_submit_entity_form',
    ];
  }
}

/**
 * Ief submit callback to make sure flexiforms work in ief.
 *
 * This overrides the standard ief entity submission.
 *
 * @param array $entity_form
 *   The entity form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 *
 * @see \flexiform_element_info_alter
 * @see \Drupal\inline_entity_form\Element\InlineEntityForm::submitEntityForm
 */
function flexiform_ief_submit_entity_form(&$entity_form, FormStateInterface $form_state) {
  $display = FlexiformEntityFormDisplay::collectRenderDisplay($entity_form['#entity'], $entity_form['#form_mode']);

  $inline_form_handler = InlineEntityForm::getInlineFormHandler($entity_form['#entity_type']);
  $inline_form_handler->entityFormSubmit($entity_form, $form_state);

  // Submit flexiform components.
  if ($display instanceof FlexiformEntityFormDisplay) {
    $display->formSubmitComponents($entity_form, $form_state);
  }

  // @todo: Find a way of deferring the saving of form entities to later. Possibly
  //        consider storing the FormEntityManager on the entity to be saved in
  //        a _insert or _update hook.
  if ($entity_form['#save_entity']) {
    $inline_form_handler->save($entity_form['#entity']);

    // Only do this if we have a flexiform.
    if ($display instanceof FlexiformEntityFormDisplay) {
      $display->saveFormEntities($entity_form, $form_state);
    }
  }
}

/**
 * Ajax element process to add our library.
 *
 * Can be added to any element that accepts ajax commands.
 */
function flexiform_ajax_form_process(&$element, FormStateInterface $form_state, &$complete_form) {
  if (isset($element['#flexiform_ajax_processed'])) {
    return $element;
  }

  // Initialize #flexiform_ajax_processed, so we do not process this element
  // again.
  $element['#flexiform_ajax_processed'] = FALSE;

  // Nothing to do if there are no Ajax settings.
  if (empty($element['#ajax'])) {
    return $element;
  }

  if (isset($element['#ajax']['event'])) {
    $element['#attached']['library'][] = 'flexiform/flexiform.ajax';
  }

  return $element;
}
